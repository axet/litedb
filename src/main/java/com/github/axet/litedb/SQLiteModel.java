package com.github.axet.litedb;

public interface SQLiteModel {

    /**
     * get item uniq id
     * 
     * @return
     */
    public Long getId();

    /**
     * set item uniq id
     * 
     * @param id
     */
    public void setId(Long id);

    /**
     * has this item been changed, but not stored to the db? need to mark
     * internal state changed, but it is too expensive to save every change to
     * db (so we have to save it on app exit)
     * 
     * @return
     */
    public boolean getMark();

    /**
     * has this item been changed, but not stored to the db? need to mark
     * internal state changed, but it is too expensive to save every change to
     * db (so we have to save it on app exit)
     * 
     * set mark state
     * 
     * @param b
     */
    public void setMark(boolean b);

}
