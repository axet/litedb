package com.github.axet.litedb;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteJob;

public abstract class SQLiteJobNoResult extends SQLiteJob<Object> {

    @Override
    protected Object job(SQLiteConnection connection) throws Throwable {
        jobNoResult(connection);
        return null;
    }

    protected abstract void jobNoResult(SQLiteConnection connection) throws Throwable;
}
