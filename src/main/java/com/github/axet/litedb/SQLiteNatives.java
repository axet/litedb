package com.github.axet.litedb;

import java.io.File;

import com.almworks.sqlite4java.SQLite;
import com.github.axet.mavennatives.MavenNatives;

public class SQLiteNatives {

    static {
        MavenNatives.mavenNatives("sqlite4java", new MavenNatives.LoadNatives() {
            @Override
            public void setPath(String libraryName, File targetFile) {
                System.setProperty(SQLite.LIBRARY_PATH_PROPERTY, targetFile.getParent());
            }
        });
    }

    static public void init() {
    }
}
