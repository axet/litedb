#!/bin/bash

# natives main

zip README.jar README || exit 1

mvn gpg:sign-and-deploy-file \
  -DuseAgent=true \
  -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ \
  -DrepositoryId=sonatype-nexus-staging \
  -DpomFile=libsqlite.pom \
  -Dpackaging=jar \
  -Dfile=README.jar || exit 1

mvn gpg:sign-and-deploy-file \
  -DuseAgent=true \
  -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ \
  -DrepositoryId=sonatype-nexus-staging \
  -DpomFile=libsqlite.pom \
  -Dclassifier=sources \
  -Dpackaging=jar \
  -Dfile=README.jar || exit 1

mvn gpg:sign-and-deploy-file \
  -DuseAgent=true \
  -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ \
  -DrepositoryId=sonatype-nexus-staging \
  -DpomFile=libsqlite.pom \
  -Dclassifier=javadoc \
  -Dpackaging=jar \
  -Dfile=README.jar || exit 1

## natives

mvn gpg:sign-and-deploy-file \
  -DuseAgent=true \
  -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ \
  -DrepositoryId=sonatype-nexus-staging \
  -DpomFile=libsqlite.pom \
  -Dclassifier=natives-windows-x86_64 \
  -Dpackaging=jar \
  -Dfile=sqlite4java/sqlite4java-win32-x64.jar || exit 1

mvn gpg:sign-and-deploy-file \
  -DuseAgent=true \
  -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ \
  -DrepositoryId=sonatype-nexus-staging \
  -DpomFile=libsqlite.pom \
  -Dclassifier=natives-windows-x86 \
  -Dpackaging=jar \
  -Dfile=sqlite4java/sqlite4java-win32-x86.jar || exit 1

mvn gpg:sign-and-deploy-file \
  -DuseAgent=true \
  -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ \
  -DrepositoryId=sonatype-nexus-staging \
  -DpomFile=libsqlite.pom \
  -Dclassifier=natives-mac-x86_64 \
  -Dpackaging=jar \
  -Dfile=sqlite4java/libsqlite4java-osx.jar || exit 1

mvn gpg:sign-and-deploy-file \
  -DuseAgent=true \
  -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ \
  -DrepositoryId=sonatype-nexus-staging \
  -DpomFile=libsqlite.pom \
  -Dclassifier=natives-linux-x86_64 \
  -Dpackaging=jar \
  -Dfile=sqlite4java/libsqlite4java-linux-amd64.jar || exit 1

mvn gpg:sign-and-deploy-file \
  -DuseAgent=true \
  -Durl=https://oss.sonatype.org/service/local/staging/deploy/maven2/ \
  -DrepositoryId=sonatype-nexus-staging \
  -DpomFile=libsqlite.pom \
  -Dclassifier=natives-linux-x86 \
  -Dpackaging=jar \
  -Dfile=sqlite4java/libsqlite4java-linux-i386.jar || exit 1

# main jar

mvn deploy
