#!/bin/bash

# natives

mvn install:install-file -Dfile=sqlite4java/libsqlite4java-linux-amd64.jar \
  -DpomFile=libsqlite.pom \
  -Dpackaging=jar \
  -Dclassifier=natives-linux-x86_64 || exit 1

mvn install:install-file -Dfile=sqlite4java/libsqlite4java-linux-i386.jar \
  -DpomFile=libsqlite.pom \
  -Dpackaging=jar \
  -Dclassifier=natives-linux-x86 || exit 1

mvn install:install-file -Dfile=sqlite4java/libsqlite4java-win32-x64.jar \
  -DpomFile=libsqlite.pom \
  -Dpackaging=jar \
  -Dclassifier=natives-windows-x86_64 || exit 1

mvn install:install-file -Dfile=sqlite4java/libsqlite4java-win32-x86.jar \
  -DpomFile=libsqlite.pom \
  -Dpackaging=jar \
  -Dclassifier=natives-windows-x86 || exit 1

mvn install:install-file -Dfile=sqlite4java/libsqlite4java-osx.jar \
  -DpomFile=libsqlite.pom \
  -Dpackaging=jar \
  -Dclassifier=natives-mac-x86_64 || exit 1

# main jar

mvn install || exit 1
