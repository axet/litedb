#!/bin/bash

(cd sqlite4java && mv libsqlite4java-osx.jnilib libsqlite4java.jnilib && jar cf libsqlite4java-osx.jar libsqlite4java.jnilib) || exit 1

(cd sqlite4java && mv libsqlite4java-linux-i386.so libsqlite4java.so && jar cf libsqlite4java-linux-i386.jar libsqlite4java.so) || exit 1
(cd sqlite4java && mv libsqlite4java-linux-amd64.so libsqlite4java.so && jar cf libsqlite4java-linux-amd64.jar libsqlite4java.so) || exit 1

(cd sqlite4java && mv sqlite4java-win32-x64.dll sqlite4java.dll && jar cf sqlite4java-win32-x64.jar sqlite4java.dll) || exit 1
(cd sqlite4java && mv sqlite4java-win32-x86.dll sqlite4java.dll && jar cf sqlite4java-win32-x86.jar sqlite4java.dll) || exit 1
