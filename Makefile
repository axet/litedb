all:

install: unpack
	./pack.sh
	./install.sh
	rm -rf sqlit4java

sqlite4java-282.zip:
	wget http://sqlite4java.googlecode.com/files/sqlite4java-282.zip

unpack: sqlite4java-282.zip
	unzip -o sqlite4java-282.zip -d .
	rm -rf sqlite4java
	mv sqlite4java-282 sqlite4java

deploy: unpack
	./pack.sh
	./deploy.sh
	rm -rf sqlit4java
